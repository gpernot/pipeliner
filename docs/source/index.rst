.. Pipeliner documentation master file

   API is generated with :
   sphinx-apidoc -M -f -o docs/source/API pipeliner

   Build the doc with :
   python setup.py build_sphinx

Welcome to Pipeliner's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Introduction

   API/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
