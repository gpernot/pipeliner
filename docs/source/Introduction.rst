############
Architecture
############

********
Protocol
********

#######
Running
#######

To start all services at once ::

  python standalone.py

*************
Configuration
*************

Copy ```pipeliner/config.py.defaults``` to ```pipeliner/config.py``` and edit as needed.

**********
API server
**********

API server listens for incoming HTTP requests.

Run it with ::

  FLASK_APP=pipeliner/api/server.py flask run


******
Worker
******

Worker listens to rabbitmq queues for job submissions.
It spawns `Job` objects that will process a pipeline using a defined `Scheduler`

Run it with ::

  python pipeliner/queue/workers.py

**********
Web server
**********

Serves websocket connections.

Run it with ::

  FLASK_APP=pipeliner/web/server.py flask run
  python pipeliner/api/websocket.py

#######
Testing
#######


