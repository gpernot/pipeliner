import json
import jwt
import uuid
from urllib.request import urlopen, Request
import urllib.parse
import random

from pipeliner import config
from pipeliner.logger import logging

api_server = getattr(config, 'api_server', 'localhost')
api_port = getattr(config, 'api_port', 5000)
api_secret = getattr(config, 'application_secret', 'snakeoil')

class TestPipeline(object):

    def test_user(self):
        values = {
            'parameters': json.dumps( {
                'username': str(uuid.uuid4()),
                'password': 's3cret'
            })
        }

        data = urllib.parse.urlencode(values)

        _jwt = jwt.encode({'user_id': 0}, api_secret, algorithm='HS256')
        headers = {'Authorization': b'Bearer %s' % _jwt}

        req = Request('http://%s:%s/user/create' % (api_server, api_port),
                      data.encode('ascii'),
                      headers,
                      method='POST')

        with urlopen(req) as resp:
            logging.debug('USER CREATE %s' % resp.read())

    def test_create(self):

        values = {
            'parameters': json.dumps( {
                'name': 'dummy',
                'description': 'Dummy task. Do nothing, but do it well.',
                'executable': {
                    'exec_path': 'sleep',
                    'parameters': ['5']
                },
                'resources': {
                    'cores': 1,
                    'threads': 1,
                    'memory': '1 MB'
                }
            })
        }

        _jwt = jwt.encode({'user_id': 0, 'foo': 'bar'}, api_secret, algorithm='HS256')
        headers = {'Authorization': b'Bearer %s' %_jwt}

        data = urllib.parse.urlencode(values)

        req = Request('http://%s:%s/task/create' % (api_server, api_port),
                      data.encode('ascii'),
                      headers=headers,
                      method='POST')
        with urlopen(req) as resp:
            logging.debug('TASK CREATE %s' % resp.read())
