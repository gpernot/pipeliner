import json
import jwt
import os
from pipeliner import model
from pipeliner.api import app
from pipeliner.logger import logging
from pipeliner import config
from .some_tasks import *
from .some_schedulers import *

api_secret = getattr(config, 'application_secret', 'snakeoil')
_jwt =jwt.encode({'user_id': 0}, api_secret, algorithm='HS256')
auth_header = {'Authorization': b'Bearer %s' % _jwt}

class TestScheduler(object):

    def test_step(self):
        #r = dummy_scheduler.step(dummy_task)
        #assert r == True

        #r = local_scheduler.step(sleep_task)
        #assert r == 0
        pass

    def test_errors(self):
        app.testing = True
        client = app.test_client()

        r = client.post('/scheduler/create',
                        data={
                            'parameters': json.dumps({
                                'unknown parameter': 'foo',
                                'description': 'Local scheduler. Do it in a process',
                                'base_path': '/tmp',
                                '_type': 'LocalScheduler'
                            }),
                        },
                        headers=auth_header)

        assert r.status_code == 400
        resp = json.loads(r.data.decode('utf-8'))
        assert 'error' in resp


    # def test_run(self):
    #     results = local_scheduler.run(test1_pipeline)
    #     logging.debug(results)
    #     assert results == [0, [0] * 8]

    def test_pbs(self):
        """
        Test for PBSScheduler.step
        """

        app.testing = True
        client = app.test_client()

        r = client.post('/task/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'hostname',
                                'description': 'Who are you ?',
                                'executable': {
                                    'exec_path': '/bin/hostname',
                                    'parameters': []
                                },
                                'resources': {
                                    'cores': 1,
                                    'threads': 1,
                                    'memory': '1 MB'
                                }
                            })
                        },
                        headers=auth_header)
        logging.debug(r.data)
        task = json.loads(r.data.decode('utf-8'))
        assert task['result'] == 'ok'
        assert 'id' in task

        r = client.post('/pipeline/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'pbs_pipeline_test_step',
                                'temporary': True,
                                'tasks': [task['id']]
                            })
                        },
                        headers=auth_header)
        pipeline = json.loads(r.data.decode('utf-8'))
        assert pipeline['result'] == 'ok'
        assert 'id' in pipeline

        r = client.post('/scheduler/create',
                        data={
                            'parameters': json.dumps({
                                '_type': 'PBSScheduler',
                                'name': 'pbs',
                                'description': 'PBS scheduler for the cluster.',
                                'pipeline_id': pipeline['id'],
                                'base_path': '/tmp',
                                'server': 'cluster.lam.fr',
                                'username': os.getlogin(),
                                'privateKeyFile': os.path.expanduser('~/.ssh/id_rsa')
                            })
                        },
                        headers=auth_header)
        scheduler = json.loads(r.data.decode('utf-8'))
        assert scheduler['result'] == 'ok'
        assert 'id' in scheduler

        r = client.post('/job/run',
                        data={
                            'parameters': json.dumps({
                                'scheduler': scheduler['id']
                            })
                        },
                        headers=auth_header)

    # -------------------------------------------------------------------------

    def test_pbs_parallel(self):
        """
        Test for PBSScheduler.parallel with 4 tasks
        """

        app.testing = True
        client = app.test_client()

        r = client.post('/task/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'hostname',
                                'description': 'Who are you ?',
                                'executable': {
                                    'exec_path': '/bin/hostname',
                                    'parameters': []
                                },
                                'resources': {
                                    'cores': 1,
                                    'threads': 1,
                                    'memory': '1 MB'
                                }
                            })
                        },
                        headers=auth_header)
        logging.debug(r.data)
        task = json.loads(r.data.decode('utf-8'))
        assert task['result'] == 'ok'
        assert 'id' in task

        r = client.post('/pipeline/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'pbs_pipeline_test_parallel',
                                'temporary': True,
                                'tasks': [task['id']]*4
                            })
                        },
                        headers=auth_header)
        pipeline = json.loads(r.data.decode('utf-8'))
        assert pipeline['result'] == 'ok'
        assert 'id' in pipeline

        r = client.post('/scheduler/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'pbs',
                                '_type': 'PBSScheduler',
                                'description': 'PBS scheduler for the cluster.',
                                'pipeline_id': pipeline['id'],
                                'base_path': '/tmp',
                                'server': 'cluster.lam.fr',
                                'username': os.getlogin(),
                                'privateKeyFile': os.path.expanduser('~/.ssh/id_rsa')
                            })
                        },
                        headers=auth_header)
        scheduler = json.loads(r.data.decode('utf-8'))
        assert scheduler['result'] == 'ok'
        assert 'id' in scheduler

        r = client.post('/job/run',
                        data={
                            'parameters': json.dumps({
                                'scheduler': scheduler['id']
                            })
                        },
                        headers=auth_header)

# -----------------------------------------------------------------------------

    def test_ssh(self):
        """
        Test for SSHScheduler.step
        """

        app.testing = True
        client = app.test_client()

        r = client.post('/task/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'hostname',
                                'description': 'Who are you ?',
                                'executable': {
                                    'exec_path': '/bin/hostname',
                                    'parameters': []
                                },
                                'resources': {
                                    'cores': 1,
                                    'threads': 1,
                                    'memory': '1 MB'
                                }
                            })
                        },
                        headers=auth_header)
        logging.debug(r.data)
        task = json.loads(r.data.decode('utf-8'))
        assert task['result'] == 'ok'
        assert 'id' in task

        r = client.post('/pipeline/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'ssh_pipeline_test_step',
                                'temporary': True,
                                'tasks': [task['id']]
                            })
                        },
                        headers=auth_header)
        pipeline = json.loads(r.data.decode('utf-8'))
        assert pipeline['result'] == 'ok'
        assert 'id' in pipeline

        r = client.post('/scheduler/create',
                       data={
                           'parameters': json.dumps({
                               '_type': 'SSHScheduler',
                               'name': 'ssh',
                               'description': 'SSH scheduler for a distant server.',
                               'pipeline_id': pipeline['id'],
                               'base_path': '/tmp/',
                               'server': 'localhost',
                               'username': os.getlogin(),
                               'privateKeyFile': os.path.expanduser('~/.ssh/id_rsa')
                           })
                       },
                       headers=auth_header)
        scheduler = json.loads(r.data.decode('utf-8'))
        assert scheduler['result'] == 'ok'
        assert 'id' in scheduler

        r = client.post('/job/run',
                        data={
                            'parameters': json.dumps({
                                'scheduler': scheduler['id']
                            })
                        },
                       headers=auth_header)
    # -------------------------------------------------------------------------

    def test_ssh_parallel(self):
        """
        Test for SSHScheduler.parallel with 4 tasks
        """

        app.testing = True
        client = app.test_client()

        r = client.post('/task/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'hostname',
                                'description': 'Who are you ?',
                                'executable': {
                                    'exec_path': '/bin/hostname',
                                    'parameters': []
                                },
                                'resources': {
                                    'cores': 1,
                                    'threads': 1,
                                    'memory': '1 MB'
                                }
                            })
                        },
                       headers=auth_header)
        logging.debug(r.data)
        task = json.loads(r.data.decode('utf-8'))
        assert task['result'] == 'ok'
        assert 'id' in task

        r = client.post('/pipeline/create',
                        data={
                            'parameters': json.dumps({
                                'name': 'ssh_pipeline_test_parallel',
                                'temporary': True,
                                'tasks': [task['id']]*4
                            })
                        },
                       headers=auth_header)
        pipeline = json.loads(r.data.decode('utf-8'))
        assert pipeline['result'] == 'ok'
        assert 'id' in pipeline

        r = client.post('/scheduler/create',
                       data={
                           'parameters': json.dumps({
                               'name': 'ssh',
                               '_type': 'SSHScheduler',
                               'description': 'SSH scheduler for the cluster.',
                               'pipeline_id': pipeline['id'],
                               'base_path': '/tmp/',
                               'server': 'localhost',
                               'username': os.getlogin(),
                               'privateKeyFile': os.path.expanduser('~/.ssh/id_rsa')
                           })
                       },
                       headers=auth_header)
        scheduler = json.loads(r.data.decode('utf-8'))
        assert scheduler['result'] == 'ok'
        assert 'id' in scheduler

        r = client.post('/job/run',
                        data={
                            'parameters': json.dumps({
                                'scheduler': scheduler['id']
                            })
                        },
                       headers=auth_header)
