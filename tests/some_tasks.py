from pipeliner import model

dummy_task = model.Task(name='dummy',
                        description='Dummy task. Do nothing, but do it well.',
                        executable={
                            'exec_path': 'true',
                            'parameters': []
                        },
                        resources={
                            'cores': 1,
                            'threads': 1,
                            'memory': '1 MB'
                        })

sleep_task = model.Task(name='sleep',
                        description='Sleep task. Do nothing, and do it slowly.',
                        executable={
                            'exec_path': '/bin/sleep',
                            'parameters': ['2']
                        },
                        resources={
                            'cores': 1,
                            'threads': 1,
                            'memory': '1 MB'
                        })

hostname_task = model.Task(name='hostname',
                        description='Hostname task.',
                        executable={
                            'exec_path': '/bin/hostname',
                            'parameters': []
                        },
                        resources={
                            'cores': 1,
                            'threads': 1,
                            'memory': '1 MB'
                        })
