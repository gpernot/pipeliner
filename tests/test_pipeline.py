import json
import jwt
from pprint import pprint
from pipeliner import model
from pipeliner.api import app
from pipeliner.logger import logging
from pipeliner import config
from .some_tasks import *
from .some_schedulers import *

import time

api_secret = getattr(config, 'application_secret', 'snakeoil')
_jwt =jwt.encode({'user_id': 0}, api_secret, algorithm='HS256')
auth_header = {'Authorization': b'Bearer %s' % _jwt}

class TestPipeline(object):

    def test_create(self):
        app.testing = True
        client = app.test_client()

        #
        # test /task/create
        #
        r = client.post('/task/create',
                        data={
                            'parameters': json.dumps( {
                                'name': 'dummy',
                                'description': 'Dummy task. Do nothing, but do it well.',
                                'executable': {
                                    'exec_path': 'sleep',
                                    'parameters': ['5']
                                },
                                'resources': {
                                    'cores': 1,
                                    'threads': 1,
                                    'memory': '1 MB'
                                }})
                        },
                        headers=auth_header)
        logging.debug(r.data)
        task = json.loads(r.data.decode('utf-8'))
        pprint(task)
        assert task['result'] == 'ok'
        assert 'id' in task

        #
        # test /pipeline/create
        #
        r = client.post('/pipeline/create',
                        data={
                            'parameters': json.dumps( {
                                'name': 'foo',
                                'temporary': True,
                                'tasks': [ task['id'], [task['id']] * 3 ]
                            })
                        },
                        headers=auth_header)
        pipeline = json.loads(r.data.decode('utf-8'))
        assert pipeline['result'] == 'ok'
        assert 'id' in pipeline

        #
        # test /scheduler/create
        #
        r = client.post('/scheduler/create',
                        data={
                            'parameters': json.dumps( {
                                '_type': 'LocalScheduler',
                                'name': 'local',
                                'description': 'Local scheduler. Do it in a process',
                                'pipeline_id': pipeline['id'],
                                'base_path': '/tmp',
                            })
                        },
                        headers=auth_header)
        scheduler = json.loads(r.data.decode('utf-8'))
        pprint(scheduler)
        assert scheduler['result'] == 'ok'
        assert 'id' in scheduler

        #
        # test /job/run
        #
        r = client.post('/job/run',
                        data={
                            'parameters': json.dumps({
                                'scheduler': scheduler['id']
                            })
                        },
                        headers=auth_header)
        job = json.loads(r.data.decode('utf-8'))
        pprint(job)
        assert job['result'] == 'ok'
        assert 'id' in job

        # wait job
        r = client.get('/job/%s/wait' % job['id'],
                       headers=auth_header)

        #
        # take 2
        #
        r = client.post('/job/run',
                        data={
                            'parameters': json.dumps({
                                'scheduler': scheduler['id']
                            })
                        },
                        headers=auth_header)
        job = json.loads(r.data.decode('utf-8'))
        assert job['result'] == 'ok'
        assert 'id' in job

        time.sleep(1)
        r = client.get('/job/%s/cancel' % job['id'],
                       headers=auth_header)

        #
        # test /job/status
        #
        for i in range(2):
            r = client.get('/job/%s' % job['id'],
                           headers=auth_header)
            status = json.loads(r.data.decode('utf-8'))
            time.sleep(1)
        # TODO: should wait job-completion notification

        #
        # test /*/delete
        #
        # r = client.delete('/pipeline/%s' % pipeline['id'])
        # r = client.delete('/scheduler/%s' % scheduler['id'])
        # r = client.delete('/task/%s' % task['id'])
        # r = client.delete('/job/%s' % job['id'])

    def test_error(self):
        app.testing = True
        client = app.test_client()

        r = client.post('/pipeline/create',
                        data={
                            'parameters': json.dumps( {
                                'name': 'foo',
                                'temporary': True,
                                'tasks': [ 0, 1, 2],
                                '_invalid_parameter_': 'foo'
                            })
                        },
                        headers=auth_header)

        assert r.status_code == 400
        resp = json.loads(r.data.decode('utf-8'))
        assert 'error' in resp

    def test_delete(self):
        app.testing = True
        client = app.test_client()

        r = client.delete('/bad/address',
                          headers=auth_header)
        assert r.status_code == 404

        r = client.delete('/pipeline/bad_db_id',
                          headers=auth_header)
        assert r.status_code == 404

        r = client.delete('/task/-1',
                          headers=auth_header)
        assert r.status_code == 404

        r = client.post('/task/create',
                        data={
                            'parameters': json.dumps( {
                                'name': 'dummy',
                                'description': 'Dummy task. Do nothing, but do it well.',
                                'executable': {
                                    'exec_path': 'true',
                                    'parameters': []
                                },
                                'resources': {
                                    'cores': 1,
                                    'threads': 1,
                                    'memory': '1 MB'
                                }})
                        },
                        headers=auth_header)
        task = json.loads(r.data.decode('utf-8'))
        _id = task['id']
        r = client.delete('/scheduler/%s' % _id,
                          headers=auth_header)
        assert r.status_code == 400
