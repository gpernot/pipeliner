from pipeliner import model

dummy_scheduler = model.DummyScheduler(name='dummy',
                                       description='Dummy scheduler. Just pretend to do the job.',
                                       base_path='/tmp')

local_scheduler = model.LocalScheduler(name='local',
                                       description='Local scheduler. Do it in a process',
                                       base_path='/tmp')

pbs_scheduler = model.PBSScheduler(name='pbs',
                                       description='PBS scheduler for the cluster.',
                                       base_path='/tmp')
