import json
import jwt
from pipeliner import model
from pipeliner.api import app
from pipeliner import config

from .some_tasks import *

api_secret = getattr(config, 'application_secret', 'snakeoil')
_jwt =jwt.encode({'user_id': 0}, api_secret, algorithm='HS256')
auth_header = {'Authorization': b'Bearer %s' % _jwt}

class TestTask(object):

    def test_run(self):
        pass

    def test_errors(self):
        app.testing = True
        client = app.test_client()

        r = client.post('/task/create',
                        data={
                            'parameters': json.dumps( {
                                'name': 'dummy',
                                'description': 'Dummy task. Do nothing, but do it well.',
                                'executable': {
                                    'exec_path': 'true',
                                    'parameters': []
                                },
                                'resources': {
                                    'cores': 1,
                                    'threads': 1,
                                    'memory': '1 MB'
                                },
                                'invalid parameter': 'foo'
                            })
                        },
                        headers=auth_header)

        assert r.status_code == 400
        resp = json.loads(r.data.decode('utf-8'))
        assert 'error' in resp

