import json
import time
from flask import request, make_response, g
import pipeliner
from pipeliner.api import app
from pipeliner.logger import logging
from pipeliner.model.status import *
from pipeliner.database import database
from pipeliner.queue import message_queue as mq
from .auth import auth_token
from pprint import pprint

@app.route('/task/create', methods=['POST'])
@auth_token
def task_create():
    """
    Create a new task.

    Path : ``/task/create``

    POST parameters [#parameters]_ :

      :name: *optional* Name of the task.
      :description: *optional* Descripition of this task and its parameters.
      :executable: How to run this task :

        :exec_path: Path to executable file.
        :parameters: Array of command-line parameters.

      :resources: *optional* Minimum resources needed for running this task :

        :cores: Number of cores needed.
        :threads: Number of threads needed.
        :memory: Memory needed.

    Example ::

     'name': 'sleep-2s',
     'description': 'Wait 2 seconds.',
     'executable': {
       'exec_path': 'sleep',
       'parameters': ['2']
     },
     'resources': {
       'cores': 1,
       'threads': 1,
       'memory': '1 MB'
     }

    .. [#parameters] Parameters are stored as a json string in ``parameters`` data field.
    """
    task = json.loads(request.form['parameters'])

    task['_type'] = 'Task'

    try:
        t = pipeliner.model.Task(**task)
    except Exception as e:
        return make_response(json.dumps({'error': str(e)}), 400)

    t.save()

    return json.dumps({'result': 'ok', 'id': t._id})


@app.route('/pipeline/create', methods=['POST'])
@auth_token
def pipeline_create():
    """
    Create a new pipeline.

    Path : ``/pipeline/create``

    POST parameters [#parameters]_ :

      :name: *optional* Name of the pipeline.
      :temporary: *optional* Whether this pipeline is temporary. If so, it will
        be deleted on completion.
      :tasks: Array of tasks id describing the pipeline.

    Example ::

      'name': 'foo',
      'temporary': True,
      'tasks': [ 1, [2, 2, 3, 3, 4], 5]
    """
    pipeline = json.loads(request.form['parameters'])

    pipeline['_type'] = 'Pipeline'
    try:
        p = pipeliner.model.Pipeline(**pipeline)
    except Exception as e:
        return make_response(json.dumps({'error': str(e)}), 400)

    p.save()

    return json.dumps({'result': 'ok', 'id': p._id})


@app.route('/scheduler/create', methods=['POST'])
@auth_token
def scheduler_create():
    """
    Create a new scheduler.

    Path : ``/scheduler/create``

    POST parameters [#parameters]_ :

      :_type: Type of scheduler to create. Currently, one of :
        ``DummyScheduler``, ``LocalScheduler``, ``PBSScheduler``.
      :name: *optional* Name of this scheduler.
      :description: *optional* Descripition of the scheduler.
      :pipeline_id: Pipeline describing tasks to run.
      :base_path: Path where input and output files will be found.
      :parameters: *optional* Extra parameters for this scheduler class.

    Example ::

      '_type': 'LocalScheduler',
      'name': 'local',
      'description': 'Local scheduler. Do it in a process',
      'pipeline_id': 1234,
      'base_path': '/tmp',
    """
    scheduler = json.loads(request.form['parameters'])

    scheduler_class = getattr(pipeliner.model, scheduler['_type'])
    try:
        s = scheduler_class(**scheduler)
    except Exception as e:
        return make_response(json.dumps({'error': str(e)}), 400)

    s.save()

    return json.dumps({'result': 'ok', 'id': s._id})

#
# DELETE calls
#

def _delete_model(_id, _type):
    """Remove an object from database"""
    try:
        obj = database.find_one({'_id': _id})
    except:
        obj = None

    if obj is None:
        logging.error('Deleting unknown %s @ %s' %(_type, _id))
        return make_response(json.dumps({'error': 'Unknown id %s' % _id}),
                             404)
    logging.debug('delete : %s' % obj)
    if not issubclass(getattr(pipeliner.model, obj['_type']),
                      getattr(pipeliner.model, _type)):
        return make_response(json.dumps({'error': 'Type mismatch %s / %s' % (obj['_type'], _type)}),
                             400)

    database.delete_one(_id)
    return json.dumps({'result': 'ok'})

@app.route('/task/<task_id>', methods=['DELETE'])
@auth_token
def task_delete(task_id):
    """
    Delete a task.

    Path : ``/task/<task_id>``

    Parameter :

      :task_id: id of task to delete.

    HTTP methods :

      :DELETE:
    """
    if request.method == 'DELETE':
        return _delete_model(task_id, 'Task')

@app.route('/pipeline/<pipeline_id>', methods=['DELETE'])
@auth_token
def pipeline_delete(pipeline_id):
    """
    Delete a pipeline.

    Path : ``/pipeline/<pipeline_id>``

    Parameter :

      :pipeline_id: id of pipeline to delete.

    HTTP methods :

      :DELETE:
    """
    if request.method == 'DELETE':
        return _delete_model(pipeline_id, 'Pipeline')

@app.route('/scheduler/<scheduler_id>', methods=['DELETE'])
@auth_token
def scheduler_delete(scheduler_id):
    """
    Delete a scheduler.

    Path : ``/scheduler/<scheduler_id>``

    Parameter :

      :scheduler_id: id of scheduler to delete.

    HTTP methods :

      :DELETE:
    """
    if request.method == 'DELETE':
        return _delete_model(scheduler_id, 'Scheduler')

#
# /job calls
#
def _tasks_from_ids(tasks_ids):
    tasks = []
    if not tasks_ids:
        return tasks
    for i in tasks_ids:
        if isinstance(i, list):
            tasks.append(_tasks_from_ids(i))
        else:
            tasks.append(database.find_one({'_id': i}))
    return tasks

def _retrieve_status(_id):
    job = database.find_one({'_id': _id})
    if not job:
        return make_response(json.dumps({'error': 'Unknown job with _id %s' % (_id,)}),
                             400)
    if job['_type'] == 'Job':
        return json.dumps({'result': 'ok', 'status': WAITING})
    elif job['_type'] == 'JobInstance':
        return json.dumps({'result': 'ok', 'status': job['_status']})
    else:
        return make_response(json.dumps({'error': '_id %s is not a Job' % (_id,)}), 400)

@app.route('/job/<job_id>', methods=['GET', 'DELETE'])
@auth_token
def job_job_id(job_id):
    """
    Operate on a job.

    Path : ``/job/<job_id>``

    Parameter :

      :job_id: id of job to operate on.

    HTTP methods :

      :GET: Retrieve job's status
      :DELETE: Delete job
    """
    if request.method == 'GET':
        return _retrieve_status(job_id)
    elif request.method == 'DELETE':
        return _delete_model(job_id, 'Job')

@app.route('/job/<job_id>/cancel', methods=['GET'])
@auth_token
def job_job_id_cancel(job_id):
    """
    Cancel a job.

    Path : ``/job/<job_id>/cancel``

    Parameter :

      :job_id: id of job to cancel.

    HTTP methods :

      :GET:

    """

    job = database.find_one({'_id': job_id})

    if not job:
        return make_response(json.dumps({'error': 'Unknown job with _id %s' % (job_id,)}),
                             400)

    mq.job_cancel(job_id)

    return json.dumps({'result': 'ok'})

@app.route('/job/<job_id>/wait', methods=['GET'])
@auth_token
def job_job_id_wait(job_id):
    """
    Wait until a job has terminated.
    Warning : HTTP may timeout before then.
    """
    job = database.find_one({'_id': job_id})

    if not job:
        return make_response(json.dumps({'error': 'Unknown job with _id %s' % (job_id,)}),
                             400)

    while '_status' not in job or job['_status'] != STOPPED:
        time.sleep(2)
        job = database.find_one({'_id': job_id})

    return json.dumps({'result': 'ok'})

@app.route('/job/run', methods=['POST'])
@auth_token
def job_run():
    """
    Create a job and run it.

    Path : ``/job/run``

    POST parameters [#parameters]_ :

      :scheduler: `id` of scheduler that will run the pipeline.

    Example ::

      'scheduler': 3
    """
    job = json.loads(request.form['parameters'])

    job['_type'] = 'Job'
    try:
        j = pipeliner.model.Job(**job)
    except Exception as e:
        return make_response(json.dumps({'error': str(e)}), 400)

    j.save()

    mq.job_start(j._id)

    return json.dumps({'result': 'ok', 'id': j._id})

#
# user api
#

@app.route('/user/create', methods=['POST'])
@auth_token
def user_create():
    """
    Create a new user.

    Path : ``/user/create``

    POST parameters [#parameters]_ :

      :username: The username used to log in.
      :firstname: *optional* First name.
      :lastname: *optional* Last name.
      :email: *optional* E-mail address.
      :password: Hash of user password.

    Example ::

     'username': 'fbar',
     'firstname': 'Foo',
     'password' : 'a433bc4a8fc544c03f52c50cd6e1e8701bc86588bd9e90e2'
    """

    if g.user['_id'] != 0:
        logging.warning('Unauthorized user/create from user %s' % g.user)
        return make_response(json.dumps({'error': 'Only admin can create users'}),
                             403)

    user = json.loads(request.form['parameters'])
    assert '_id' not in user # prevent bad injections

    if database.find_one({'username': user['username']}):
        logging.warning('A user with that username already exists %s' % g.user)
        return make_response(json.dumps({'error': 'Username in use'}),
                             403)

    user['_type'] = 'User'
    try:
        u = pipeliner.model.User(**user)
    except Exception as e:
        logging.error(str(e))
        return make_response(json.dumps({'error': str(e)}), 400)
    u.save()
    return json.dumps({'result': 'ok', 'id': u._id})


@app.route('/user/<user_id>/update', methods=['POST'])
@auth_token
def user_update(user_id):
    """
    Update user.

    Path : ``/user/<user_id>/update``

    Parameters :

      :user_id: id of user to update.

    POST parameters [#parameters]_ :

      :username: *optional* The username used to log in.
      :firstname: *optional* First name.
      :lastname: *optional* Last name.
      :email: *optional* E-mail address.
      :password: *optional* Hash of user password.

    Example ::

     'username': 'foobar',
     'lastname': 'Bar',
    """
    pass

@app.route('/user/<user_id>', methods=['DELETE'])
@auth_token
def user_delete(user_id):
    """
    Delete a user.

    Path : ``/user/<user_id>``

    Parameter :

      :user_id: id of user to delete.

    HTTP methods :

      :DELETE:
    """
    if g.user['_id'] == 0:
        logging.warning('Unauthorized user/delete from user %s' % g.user)
        return make_response(json.dumps({'error': 'Only admin can delete users'}),
                             403)

    if request.method == 'DELETE':
        return _delete_model(user_id, 'User')

@app.route('/user/login', methods=['POST'])
def user_login():
    """
    Login. Get an authentication token.

    Path : ``/user/login``

    POST parameters [#parameters]_ :

      :username: The username used to log in.
      :password: User password.

    Example ::

     'username': 'foobar',
     'password': 's3cr3t',
    """
    pass

@app.route('/user/logout', methods=['GET'])
@auth_token
def user_logout():
    """
    Login. Invalidate authentication token.

    Path : ``/user/logout``
    """
    pass


