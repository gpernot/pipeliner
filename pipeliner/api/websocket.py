#!/usr/bin/env python

import json
import asyncio
import aio_pika
import websockets

from pprint import pprint
from pipeliner import config
from pipeliner.logger import logging
from pipeliner.database import database


class Notification(object):
    def __init__(self, server, port):
        self.start_server = websockets.serve(self.notifications, server, port)
        self.messages = []
        self.message_sem = asyncio.Semaphore(0)

    async def rabbit_consumer(self):
        username = getattr(config, 'rabbit_username', 'guest')
        password = getattr(config, 'rabbit_password', 'guest')
        server = getattr(config, 'rabbit_server', 'localhost')

        connection = await aio_pika.connect_robust("amqp://%s:%s@%s/" %
                                                   (username, password, server))
        channel = await connection.channel()
        exchange = await channel.declare_exchange('pipeliner', aio_pika.ExchangeType.TOPIC)
        self.queue = await channel.declare_queue(name='update_events', durable=True)
        await self.queue.bind(exchange, routing_key='#.update')
        await self.queue.consume(self.on_message)

    def on_message(self, message):
        with message.process():
            self.messages.append(message.body.decode('utf-8'))
        self.message_sem.release()

    def run(self):
        asyncio.ensure_future(self.start_server)
        asyncio.ensure_future(self.rabbit_consumer())

    async def notifications(self, websocket, path):
        while True:
            await self.message_sem.acquire()
            message = self.messages.pop()
            pprint(message)
            await websocket.send(message)

def ws_server(server, port):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    n = Notification(server, port)
    n.run()
    loop.run_forever()

if __name__ == '__main__':
    ws_server(getattr(config, 'websocket_server', 'localhost'),
              int(getattr(config, 'websocket_port', 5003)))
    print('done')

