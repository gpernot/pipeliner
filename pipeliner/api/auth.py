"""
User authentication and authorization
"""
import json
import jwt
import pprint
from flask import g, request, make_response
from functools import wraps

from pipeliner.database import database
from pipeliner.logger import logging
from pipeliner import config

_secret = getattr(config, 'application_secret', 'snakeoil')

def auth_token(func):
    """
    Get a JWT token from request header.
    Set `user` variable.
    """
    @wraps(func)
    def set_auth_token(*args, **kwargs):
        if 'Authorization' not in request.headers:
            logging.warning('Missing authentication token')
            return make_response(json.dumps({'error': 'Missing authentication token'}),
                                 400)
        try:
            _jwt = request.headers.get('Authorization').split(' ',1)[1].strip()
            logging.warning('REQUEST %s' % _jwt)
            auth = jwt.decode(_jwt, _secret, 'HS256')
        except Exception as e:
            logging.warning('REQUEST AUTH error : %s' % (str(e)))
            return make_response(json.dumps({'error': 'Invalid token %s : %s' % (_jwt, str(e))}),
                                 403)
        if auth['user_id'] == 0:
            # special case : admin login
            g.user = {'_id': 0,
                      'username': 'admin'}
        else:
            try:
                g.user = database.find_one({'_id': auth['user_id']})
            except:
                logging.warning('Unknown user id %s' % auth['user_id'])
                return make_response(json.dumps({'error': 'Unknown user id %s' % auth['user_id']}),
                                     403)
        logging.debug('user authenticated : %s' % g.user)
        return func(*args, **kwargs)
    return set_auth_token

def need_authorization(acl):
    def auth_decorator(func):
        def auth_wrapper():
            pass
        return auth_wrapper
    return auth_decorator
