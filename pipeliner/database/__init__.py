from pipeliner import config
import importlib

#from .userdict import database as db_userdict
#from .mongo import database as db_mongo
driver = importlib.import_module(getattr(config, 'database_driver', 'pipeliner.database.mongo'))
database = driver.database
