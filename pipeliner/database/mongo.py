import pymongo
import copy
from bson.objectid import ObjectId
from pipeliner.logger import logging
from pipeliner import config

class Database(object):
    """
    MongoDB connector.
    """

    def __init__(self, server=None, database=None):
        """
        A connection to a MongoDB server.
        `server` is in URI format. https://docs.mongodb.com/manual/reference/connection-string/
        """
        if server is None:
            self.server = 'mongodb://localhost'
        else:
            self.server = server
        self.client = pymongo.MongoClient(self.server)
        if database is None:
            self.db = self.client['pipeliner_v1']
        else:
            self.db = self.client[database]
        self.collection = self.db['objects']

    def insert_one(self, doc):
        """
        Insert a new document into database.
        If a document with the same _id exists, replace it.
        """
        if '_id' in doc and doc['_id']:
            # replace an object with same _id
            # we have to remove '_id' field from $set, as doc['_id'] is a str
            # instead of ObjectId
            _doc = dict([(k,v) for k,v in doc.items() if k != '_id'])
            self.collection.update_one({'_id': ObjectId(doc['_id'])},
                                       {'$set': _doc})
        else:
            if doc['_id'] is None:
                del doc['_id']
            r = self.collection.insert_one(doc)
            doc['_id'] = str(r.inserted_id)
        return doc['_id']

    def update_one(self, _filter, update):
        if '_id' in _filter:
            _filter['_id'] = ObjectId(_filter['_id'])
        self.collection.update_one(_filter, {'$set' : update})

    def delete_one(self, _id):
        self.collection.delete_one({'_id': ObjectId(_id)})

    def find_one(self, doc):
        if '_id' in doc:
            _d = copy.deepcopy(doc)
            _d['_id'] = ObjectId(doc['_id'])
            r = self.collection.find_one(_d)
        else:
            r = self.collection.find_one(doc)
        if r:
            r['_id'] = str(r['_id'])
        return r

database = Database(getattr(config, 'mongo_connect', 'mongodb://localhost'),
                    getattr(config, 'mongo_database', 'pipeliner_v1'))
