#!/usr/bin/env python

import json
from pprint import pprint

from pipeliner.queue import Consumer
from pipeliner.model import JobInstance
from pipeliner.logger import logging
from pipeliner.database import database

class _Registry(object):
    __instance = None
    __consumers = []
    def __new__(cls):
        if _Registry.__instance == None:
            _Registry.__instance = super().__new__(cls)
        return _Registry.__instance

    def register(self, consumer):
        self.__consumers.append(consumer)

    def wait_all(self):
        for c in self.__consumers:
            c.consuming_thread.join()

def job_submit_callback(ch, method, properties, body):
    """Instantiate a Job with the given Scheduler and Pipeline"""
    _id = method.routing_key.split('.')[1]
    job = database.find_one({'_id': _id})
    logging.debug('WORKERJOB : %s' % job)

    # instantiate job instance and start it
    # n.b. : JobInstance is stealing Job id
    job['_type'] = 'JobInstance'
    job_obj = JobInstance(**job)
    job_obj.run()

def updates_callback(ch, method, properties, body):
    logging.debug('Message queue @%s : %s' % (method, body))

registry = _Registry()

registry.register( Consumer('job.*.start', job_submit_callback) )
registry.register( Consumer('*.update', updates_callback) )

if __name__ == '__main__':
    registry.wait_all()
