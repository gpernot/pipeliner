import pika
import json
import threading
from pipeliner import config
from pipeliner.logger import logging

class MessageQueue(object):

    def __init__(self):
        server = getattr(config, 'rabbit_server', 'localhost')
        username = getattr(config, 'rabbit_username', 'guest')
        password = getattr(config, 'rabbit_password', 'guest')

        cred = pika.PlainCredentials(username, password)
        parameters = pika.ConnectionParameters(host=server, credentials=cred)
        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange='pipeliner',
                                      exchange_type='topic')
        self.lock = threading.Lock()

class Producer(MessageQueue):

    def publish(self, key, body=None):
        self.lock.acquire()
        self.channel.basic_publish(exchange='pipeliner',
                                   routing_key=key,
                                   body=body if body else '{}',
                                   properties=pika.BasicProperties(
                                       delivery_mode = 2, # make message persistent
                                   ))
        self.lock.release()

    def job_start(self, _id):
        self.publish('job.%s.start' % _id)

    def job_cancel(self, _id):
        self.publish('job.%s.cancel' % _id)

    def model_update(self, message):
        self.publish('%s.update' % (message['_type'].lower()),
                     json.dumps(message))

class Consumer(MessageQueue):

    def __init__(self, key, callback):
        super().__init__()

        q = self.channel.queue_declare(exclusive=True)

        self.channel.queue_bind(exchange='pipeliner',
                                queue=q.method.queue,
                                routing_key=key)

        self.channel.basic_consume(callback,
                                   queue=q.method.queue,
                                   no_ack=True)

        self.consuming_thread = threading.Thread(target=self._consuming_thread)
        self.consuming_thread.start()

    def _consuming_thread(self):
        self.channel.start_consuming()

message_queue = Producer()
