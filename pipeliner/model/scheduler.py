import os
import subprocess
import threading
import json
from concurrent.futures import ThreadPoolExecutor, wait

from pipeliner import config
from pipeliner.logger import logging
from .model import Model, ModelInstance, Schema
from .status import *
from .task import TaskInstance
from .pipeline import PipelineInstance
from pipeliner.database import database
from pipeliner.queue import Consumer, message_queue as mq


class Scheduler(Model):
    """
    `pipeline_id`: pipeline model to instantiate
    """
    __schema__ = Model.__schema__ + Schema({'_type': 'Scheduler',
                                            'name': None,
                                            'description': None,
                                            'pipeline_id': None,
                                            'base_path': None,
                                            'parameters': None})

class SchedulerInstance(ModelInstance):
    """
    `job_id`: job that has instantiated this scheduler
    """
    __schema__ = ModelInstance.__schema__ + Scheduler.__schema__ + \
                 Schema({'_type': 'SchedulerInstance',
                         'job_id': None})


    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.save() # setup self._id
        self.pipeline = PipelineInstance(pipeline_id=self.pipeline_id)

        self.wait_queue = []
        self.wait_queue_lock = threading.Lock() # serialize wait_queue accesses
        self.wait_queue_is_empty = threading.Condition()

    def _task_cb(self, ch, method, properties, body):
        # received a message with key task.<sched>.*.stopped
        # remove task from wait_queue
        _id = method.routing_key.split('.')[2]
        if _id in self.wait_queue:
            with self.wait_queue_lock:
                self.wait_queue.remove(_id)
                if not self.wait_queue:
                    with self.wait_queue_is_empty:
                        self.wait_queue_is_empty.notify()

    def _process_pipeline(self):
        """Process the pipeline"""
        Consumer('task.%s.*.stopped' % self._id, self._task_cb)
        for t in self.pipeline:
            if self._status == CANCELING:
                mq.publish('scheduler.%s.canceled' % self._id)
                logging.error('=== CANCELED')
                break
            if isinstance(t, list):
                for _t in t:
                    with self.wait_queue_lock:
                        self.wait_queue.append(_t._id)
                # start parallel tasks
                self.parallel(t)
            elif isinstance(t, TaskInstance):
                with self.wait_queue_lock:
                    self.wait_queue.append(t._id)
                # start tasks
                self.step(t)

            else:
                logging.error('Bad type for task : %s' % t)
                mq.publish('scheduler.%s.canceled' % self._id)
                raise Exception('Invalid pipeline definition : %s' % self.pipeline)
        else:
            # wait wait_queue is empty, ie : all tasks are done
            with self.wait_queue_is_empty:
                self.wait_queue_is_empty.wait_for(lambda: len(self.wait_queue) == 0)

            # notify consumers we are done
            mq.publish('scheduler.%s.stopped' % self._id)

        self.update_status(STOPPED)

    def run(self):
        """
        Use this scheduler to run the given pipeline.
        Return created tasks' ids.
        """

        self.update_status(RUNNING)
        self.thread = threading.Thread(target=self._process_pipeline)
        self.thread.start()

    def done(self):
        """
        Called when scheduler has finished to process the pipeline and all jobs
        are finished.
        """
        # TODO
        #mq.job_done(self.job_id)

    def cancel(self):
        """Cancel the running pipeline."""
        logging.debug('CANCELING %s' % self.as_dict())
        self.update_status(CANCELING)

    def step(self, task):
        """Perform one task."""
        pass

    def parallel(self, tasks):
        """Run tasks in parallel."""
        pass

class DummyScheduler(Scheduler):
    __schema__ = Scheduler.__schema__ + Schema({'_type': 'DummyScheduler'})

class DummySchedulerInstance(SchedulerInstance):
    __schema__ = SchedulerInstance.__schema__ + DummyScheduler.__schema__ + \
                 Schema({'_type': 'DummySchedulerInstance',})

    def step(self, task):
        return True

    def parallel(self, tasks):
        return [True for t in tasks]

class LocalScheduler(Scheduler):
    """
    `concurrency`: number of processes to run in parallel
    """

    __schema__ = Scheduler.__schema__ + Schema({'_type': 'LocalScheduler',
                                                'concurrency': None})

class LocalSchedulerInstance(SchedulerInstance):
    __schema__ = SchedulerInstance.__schema__ + LocalScheduler.__schema__ + \
                 Schema({'_type': 'LocalSchedulerInstance',})

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.concurrency is None:
            try:
                self.concurrency = len(os.sched_getaffinity(0))
            except:
                self.concurrency = 2

        logging.debug("Setting concurrency to %s" % self.concurrency)

        # list of futures currently running
        self.futures = []

    def step(self, task):

        if self._status == CANCELING:
            return

        args = [task.executable['exec_path']]
        args.extend(task.executable['parameters'])

        logging.debug("\tstepping... %s/%s" % (self.base_path, args))

        task.update_status(RUNNING)
        p = subprocess.Popen(args, cwd=self.base_path)
        p.wait()
        mq.publish('task.%s.%s.stopped' % (self._id, task._id))
        task.update_status(STOPPED)

        logging.debug("\tdone")
        return p.returncode

    def parallel(self, tasks):
        with ThreadPoolExecutor(max_workers=self.concurrency) as executor:
            for task in tasks:
                f = executor.submit(self.step, task=task)
                self.futures.append(f)
        results = [f.result() for f in self.futures]
        self.futures = []
        return results

class PBSScheduler(Scheduler):
    """
    `_type`: PBSScheduler.
    `server`: adress of the cluster (cluster.lam.fr).
    `username`: user's login on the server.
    `privateKeyFile`: path to the user's private key file, which will be copied on the server.
    """
    __schema__ = Scheduler.__schema__ + Schema({'_type': 'PBSScheduler',
                                                'server': None,
                                                'username': None,
                                                'privateKeyFile': '.ssh/id_rsa'})

class PBSSchedulerInstance(SchedulerInstance):

    __schema__ = SchedulerInstance.__schema__ + PBSScheduler.__schema__ + \
                 Schema({'_type': 'PBSSchedulerInstance'})


    def step(self, task):
        """
        Process one task
        """
        bash_file_name = os.path.join(self.base_path,'submit-{0}.sh'.format(task._id))

        executable = task.executable['exec_path']
        code = """#PBS -N pbs_pipeline_test
                #PBS -l nodes=1:ppn=1
                #PBS -l walltime=0:1:0
                cd $PBS_O_WORKDIR
                {0} >> out-{1}-{2}.txt
                amqp-publish --url amqp://{3} -e pipeliner -r task.{1}.{2}.stopped -b ''
                """.format(executable, self._id, task._id, config.rabbit_server)

        with open(bash_file_name, "w") as bash_file:
            bash_file.write(code)

        argsScp = ["scp","-i",
                   self.privateKeyFile,
                   bash_file_name,
                   "{0}@{1}:".format(self.username, self.server)]

        p = subprocess.Popen(argsScp)
        p.wait()

        argsQsub = ["ssh", "-i", self.privateKeyFile,
                    "{0}@{1}".format(self.username,self.server),
                    "qsub",os.path.basename(bash_file_name)]

        p = subprocess.Popen(argsQsub)
        p.wait()

        return p.returncode


    def parallel(self, tasks):
        """Run tasks in parallel with threads calling step() function."""
        results=[]
        for t in tasks:
            results.append(self.step(t))
        return results


class SSHScheduler(Scheduler):
    """
    `_type`: SSHScheduler.
    `concurrency`: number of processes to run in parallel.
    `server`: adress of the server on which pipeline will be run.
    `username`: user's login on the server.
    `privateKeyFile`: path to the user's private key file, which will be copied on the server.
    """

    __schema__ = Scheduler.__schema__ + Schema({'_type': 'SSHScheduler',
                                                'concurrency': None,
                                                'server': None,
                                                'username': None,
                                                'privateKeyFile': '.ssh/id_rsa'})

class SSHSchedulerInstance(SchedulerInstance):

    __schema__ = SchedulerInstance.__schema__ + SSHScheduler.__schema__ + \
                 Schema({'_type': 'SSHSchedulerInstance'})

    def step(self, task):
        """Process one task"""

        bash_file_name = os.path.join(self.base_path,'submit-{0}.sh'.format(task._id))
        executable = task.executable['exec_path']
        code = """cd {0}
{1} >> out-{2}-{3}.txt
amqp-publish --url amqp://{4} -e pipeliner -r task.{2}.{3}.stopped -b ''
""".format(self.base_path, executable, self._id, task._id, config.rabbit_server)

        with open(bash_file_name, "w") as bash_file:
            bash_file.write(code)

        argsScp = ["scp",
                    "-i",
                   self.privateKeyFile,
                   bash_file_name,
                   "{0}@{1}:".format(self.username, self.server)]

        argsExe = ["ssh", "-i", self.privateKeyFile,
                    "{0}@{1}".format(self.username,self.server),
                    "sh", bash_file_name]

        logging.debug('user name %s' % self.username)

        p = subprocess.Popen(argsScp)
        logging.debug('argsScp %s' % argsScp)
        p.wait()

        p = subprocess.Popen(argsExe)
        logging.debug('argsExe %s' % argsExe)
        p.wait()

        return p.returncode

    def parallel(self, tasks):
        """Run tasks in parallel with threads calling step() function."""
        with ThreadPoolExecutor(max_workers=self.concurrency) as executor:
            for task in tasks:
                f = executor.submit(self.step, task=task)
                self.futures.append(f)
        results = [f.result() for f in self.futures]
        self.futures = []
        return results
