from .model import Model, Schema

class User(Model):
    """A user of the pipeliner"""
    __schema__ = Model.__schema__ + Schema({'_type': 'user',
                                            'username': None,
                                            'firstname': None,
                                            'lastname': None,
                                            'email': None,
                                            'password': None})
