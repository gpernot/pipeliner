import json
from .model import Model, ModelInstance, Schema
from .status import *
import pipeliner
from pipeliner.logger import logging
from pipeliner.database import database
from pipeliner.queue import Consumer, message_queue as mq

class Job(Model):
    """
    `scheduler` : `id` of scheduler that will run the pipeline.
    `owner` : `id` of user that submitted that job.
    """

    __schema__ = Model.__schema__ + Schema({'_type': 'Job',
                                            'scheduler': None,
                                            'owner': None,
                                            })


class JobInstance(ModelInstance):
    """
    `scheduler_instance`: `id` of created SchedulerInstance
    `start_consumer`: whether to start a rabbitmq consumer
    """
    __schema__ = ModelInstance.__schema__ + Job.__schema__ + \
                 Schema({'_type': 'JobInstance',
                         'scheduler_instance': None})

    def __init__(self, **kwargs):
        """Create a running job"""
        super().__init__(**kwargs)
        self.save() # setup self._id

    def run(self):

        # instantiate a scheduler.
        # type is given by appending "Instance" to base model type
        _s = database.find_one({'_id' : self.scheduler})
        _s['_type'] = '%sInstance' % _s['_type']
        del _s['_id'] # force the creation of a new object of type <base>Instance
        scheduler_class = getattr(pipeliner.model, _s['_type'])
        self.scheduler_instance = scheduler_class(job_id = self._id,
                                                  **_s)

        database.update_one({'_id': self._id},
                            {'scheduler_instance': self.scheduler_instance._id})

        Consumer('job.%s.#' % self._id, self._notification_cb)
        Consumer('scheduler.%s.#' % self.scheduler_instance._id, self._scheduler_cb)

        self.scheduler_instance.run()
        self.update_status(RUNNING)

    def _notification_cb(self, ch, method, properties, body):
        """Handle messages to this instance"""
        s = json.loads(body.decode('utf-8'))
        logging.debug('JOB notification: %s' % s)
        prefix = 'job.%s.' % self._id
        message = method.routing_key[len(prefix):]
        if message == 'cancel':
            self.update_status(CANCELING)
            self.scheduler_instance.cancel()
        elif message == 'stopped':
            self.update_status(STOPPED)
        elif message == 'start':
            # we can't start a job here, as the consumer is not yet created.
            # thus, 'start' is handled in workers.py.
            # self.run()
            pass

    def _scheduler_cb(self, ch, method, properties, body):
        """Handle messages from scheduler"""
        s = json.loads(body.decode('utf-8'))
        message = method.routing_key.split('.')[2]
        if message == 'stopped':
            logging.debug('JOB notification: scheduler %s done' % self.scheduler_instance._id)
            self.update_status(STOPPED)
        elif message == 'canceled':
            logging.debug('JOB notification: scheduler %s canceled' % self.scheduler_instance._id)
            self.update_status(CANCELED)
        else:
            logging.debug('JOB notification: UNKNOWN MESSAGE %s' % message)

