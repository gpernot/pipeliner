# Some defined status

SPAWNING='spawning'
RUNNING='running'
PAUSED='paused'
STOPPED='stopped'
ERROR='error'
WAITING='waiting' # waiting some sub-process to complete
CANCELING='canceling'
CANCELED='canceled'
