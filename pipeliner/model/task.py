from pipeliner.logger import logging
from pipeliner.queue import message_queue as mq
from .model import Model, ModelInstance, Schema


class Task(Model):
    """Description of a task to instantiate"""
    __schema__ = Model.__schema__ + Schema({'_type': 'Task',
                                            'name': None,
                                            'description': None,
                                            'executable': None,
                                            'resources': None})

    # TODO : add per-task walltime / ppn


class TaskInstance(ModelInstance):
    """
    An actually instantiated task.
    `pipeline_id`: `id` of pipeline ownning this task
    """
    __schema__ = ModelInstance.__schema__ + Task.__schema__ + \
                 Schema({'_type': 'TaskInstance',
                         'pipeline_id': None})

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.save() # setup self._id
