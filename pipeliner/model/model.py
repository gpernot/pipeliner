import copy
import json
from pipeliner.logger import logging
from pipeliner.database import database
from pipeliner.queue import message_queue as mq


class Schema(object):
    def __init__(self, data):
        self.data = data

    def __add__(self, other):
        data = copy.deepcopy(self.data)
        data.update(other.data)
        return Schema(data)

    def __iter__(self):
        return iter(self.data)

    def keys(self):
        return self.data.keys()

    def __getitem__(self, k):
        return self.data[k]

    def __setitem__(self, k, v):
        self.data[k] = v

    def __str__(self):
        return str(self.data)


class Model(object):
    """

    """
    __schema__ = Schema({'_type': 'Model', '_id': None})

    def __init__(self, **kwargs):
        self.validate(kwargs)
        for k in self.__schema__.keys():
            v = kwargs.get(k, self.__schema__[k])
            setattr(self, k, v)

    def save(self):
        self._id = database.insert_one(self.as_dict())

    @classmethod
    def validate(cls, data):
        """Validate data"""
        for k in data.keys():
            if k not in cls.__schema__:
                raise Exception('[%s] not in %s' % (k, cls.__schema__))

        if '_type' in data:
            if data['_type'] != cls.__name__:
                raise Exception('%s != %s' % (data['_type'], cls.__name__))

        # set default values
        for k in cls.__schema__:
            if k not in data:
                data[k] = cls.__schema__[k]

    def as_dict(self):
        """Return a dict object representing `self`"""
        result = {}
        for k in self.__schema__:
            result[k] = getattr(self, k)
        return result

    def __str__(self):
        result = {}
        for k in self.__schema__.keys():
            result[k] = getattr(self, k)
        return str(result)

class ModelInstance(Model):
    """
    An running instance of a model.
    """
    __schema__ = Model.__schema__ + Schema({'_type': 'ModelInstance',
                                            '_status': None})

    @property
    def status(self):
        return self._status

    def update_status(self, value):
        if self._status != value:
            self._status = value

            # register the modification in database
            database.update_one({'_id': self._id},
                                {'_status': self._status})
            # notify rabbit of the change
            mq.model_update({'_id': self._id,
                             '_type': self._type,
                             '_status': self._status})

