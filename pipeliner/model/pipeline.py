from .model import Schema, Model, ModelInstance
from .task import TaskInstance
from .status import *
from pipeliner.logger import logging
from pipeliner.database import database


class Pipeline(Model):
    __schema__ = Model.__schema__ + Schema({'_type': 'Pipeline',
                                            'name': None,
                                            'tasks': None,
                                            'temporary': False})

    def __iter__(self):
        for t in self.tasks:
            yield t

class PipelineInstance(object):
    """
    An instance of `Pipeline`.
    Note: Not herited from `Model` as it doesn't need to be saved in database.
    """

    def __init__(self, pipeline_id):
        """
        Instantiate a pipeline given its model and (TO-BE-DONE) parameters
        """
        _p = database.find_one({'_id' : pipeline_id})

        self.pipeline_id = pipeline_id
        self._tasks = self._create_tasks_from_ids(_p['tasks'])

    def __iter__(self):
        """Iterate over tasks instances"""
        for t in self._tasks:
            yield t

    def _create_tasks_from_ids(self, tasks_ids):
        """
        Instantiate `Task`s given their model ids.
        Return the list of created tasks' ids.
        Result shape matches input shape.
        """
        tasks = []
        for t in tasks_ids:
            if isinstance(t, list):
                tasks.append(self._create_tasks_from_ids(t))
            else:
                # retrieve Task model from database and instantiate one.
                _t = database.find_one({'_id': t})
                params = {'_type': 'TaskInstance',
                          'name': _t['name'],
                          'description': _t['name'],
                          'executable': _t['executable'],
                          'resources': _t['resources'],
                          'pipeline_id': self.pipeline_id}
                __t = TaskInstance(**params)
                __t.save()
                __t.update_status(WAITING)
                tasks.append(__t)
        return tasks


