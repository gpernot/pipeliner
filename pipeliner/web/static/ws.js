var ws = new WebSocket("ws://localhost:5002/");

ws.onmessage = function (event) {
    var states = document.getElementById("states"),
	notification = JSON.parse(event.data),
	_id = notification._id,
	item = document.getElementById(_id),
	content = document.createTextNode(`
${notification._id} | ${notification._type} | ${notification._status}`);

    if (item == null) {
	var item = document.createElement("li");
	item.setAttribute('id', _id);
	item.appendChild( content );
	states.appendChild(item);
    } else {
	item.replaceChild( content,
			   item.firstChild);
    }
};

