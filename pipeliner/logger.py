import logging
logging.basicConfig(filename='pipeliner.log', level=logging.DEBUG,
                    format='%(asctime)s %(message)s')
