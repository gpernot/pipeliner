#!/usr/bin/env python

import threading
from gevent.pywsgi import WSGIServer

from pipeliner.api.server import app as api_wsgi
from pipeliner.api.websocket import ws_server
from pipeliner.web.server import app as web_wsgi
from pipeliner.queue.workers import registry

from pipeliner import config

def run_flask_server(wsgi_app, host, port):
    server = WSGIServer((host, port), wsgi_app)
    server.serve_forever()

if __name__ == '__main__' :

    servers = []

    print("Starting api server")
    t = threading.Thread(target=run_flask_server,
                         args=(api_wsgi,
                               getattr(config, 'api_server', 'localhost'),
                               getattr(config, 'api_port', 5000)))
    t.start()
    servers.append(t)

    print("Starting web server")
    t = threading.Thread(target=run_flask_server,
                         args=(web_wsgi,
                               getattr(config, 'web_server', 'localhost'),
                               getattr(config, 'web_port', 5001)))
    t.start()
    servers.append(t)

    print("Starting worker process")
    t = threading.Thread(target=registry.wait_all)
    t.start()
    servers.append(t)

    print("Starting websocket notifier")
    t = threading.Thread(target=ws_server,
                         args=(getattr(config, 'websocket_server', 'localhost'),
                               int(getattr(config, 'websocket_port', 5002))))
    t.start()
    servers.append(t)

    for t in servers:
        t.join()
