import os
from setuptools import setup
from sphinx.setup_command import BuildDoc
cmdclass = {'build_sphinx': BuildDoc}

setup(
    name = "pipeliner",
    version = "0.0.1",
    author = "CeSAM",
    description = ("CeSAM data-processing pipeline."),
    license = "GPLv3+",
    url = "http://www.lam.fr",
    packages=['pipeliner', 'tests', ],
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),

    setup_requires=['Flask>=0.12', 'pytest-runner', ],
    tests_require=['Flask>=0.12', 'pytest', ],

    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Scientific/Engineering",
        "Programming Language :: Python",
        "Framework :: Flask",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    ],
)
